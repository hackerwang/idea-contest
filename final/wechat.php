<?php
/**
  * 清华大学电子工程系 王晗
  */
require_once '../mysql_func.php';
ini_set('date.timezone','Asia/Shanghai');
error_reporting(E_ALL^E_NOTICE);
//define your token
define("TOKEN", "agsagf567asad2nmb5f67576");
//文件所在目录地址
define("webroot","http://weixin.thusast.org/");
$wechatObj = new wechatCallbackapiTest();
if (empty($_GET["echostr"]))
	$wechatObj->responseMsg();
else
	$wechatObj->valid();

class wechatCallbackapiTest
{
    public function valid()
    {
        $echoStr = $_GET["echostr"];

        //valid signature , option
        if($this->checkSignature()){
            echo $echoStr;
          exit;
        }
    }

    public function responseMsg()
    {
        //get post data, May be due to the different environments
        $postStr = $GLOBALS["HTTP_RAW_POST_DATA"];

        if (!empty($postStr)){
                
                $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
                $RX_TYPE = trim($postObj->MsgType);

                switch($RX_TYPE)
                {
                    case "text":
                        $resultStr = $this->handleText($postObj);
                        break;
                    case "event":
                        $resultStr = $this->handleEvent($postObj);
                        break;
                    default:
                        $resultStr = "Unknow msg type: ".$RX_TYPE;
                        break;
                }
                echo $resultStr;
        }else {
            echo "";
            exit;
        }
    }

    public function handleText($postObj)
    {
        $fromUsername = $postObj->FromUserName;
        $toUsername = $postObj->ToUserName;
		$createTime = $postObj->CreateTime;
        $keyword = trim($postObj->Content);
        $time = time();
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[%s]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>0</FuncFlag>
                    </xml>"; 
		$msgType = "text";
		$nowtime=date("Y-m-d G:i:s");
        if(($keyword=='0')||(!empty( $keyword )))
        {
			//处理投票
			if (strstr($keyword,'+'))
			{
				//获取投票开关
				$select_sql="SELECT value FROM number WHERE entry='voteswitch'";
				$rows=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
				$voteswitch=$rows['value'];
				if ($voteswitch!='1')
				{
					$contentStr = "Sorry, 投票尚未开始！";
					$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					echo $resultStr;
					return;
				}
				//检查是否投过票
				$select_sql="SELECT ID,time FROM voted_final WHERE from_user='$fromUsername'";
				$visittime=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
				if (!empty($visittime))
				{
					$contentStr = "投票无效！\n您已经于".$visittime['time']."投过票了。\n您的抽奖编号为：".$visittime['ID'];
					$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					echo $resultStr;
					return;
				}
				//获取投票限制
				$select_sql="SELECT value FROM number WHERE entry='mustchoose'";
				$rows=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
				$mustchoose=$rows['value'];	//必选选项数
				$select_sql="SELECT value FROM number WHERE entry='permitmore'";
				$rows=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
				$permitmore=$rows['value'];	//是否允许超出必选数
				$select_sql="SELECT value FROM number WHERE entry='mostchoose'";
				$rows=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
				$mostchoose=$rows['value'];	//最大选择数
				
				//截取选项，[0]为姓名，[1]为选项字符串
				$keywords = explode("+",$keyword);
				//实际选取的选项数检查
				$chosen = strlen($keywords[1]);
				if (($chosen<$mustchoose)||($chosen>$mostchoose)||(($permitmore == '0')&&($chosen>$mustchoose)))
				{
					$contentStr = "投票无效！\n您的选项个数不符合要求，回复“投票”查看投票说明。";
					$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
					echo $resultStr;
					return;
				}
				//取出每个选项
				FOR ($i = 0; $i < $chosen; $i++)
				{
					$options[$i] = mb_substr($keywords[1],$i,1);
					//检查选项有效性
					$select_sql="SELECT ID FROM vote WHERE fakeid='$options[$i]'";
					$testid=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
					if (empty($testid))
					{
						$contentStr = "投票无效！\n您的选项代码有误，回复“投票”查看投票说明。";
						$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
						echo $resultStr;
						return;
					}
				}
				//检查重复选项
				FOR ($i = 0; $i < $chosen-1; $i++)
				{
					FOR ($j = $i+1; $j < $chosen; $j++)
					{
						if ($options[$i]==$options[$j])
						{
							$contentStr = "投票无效！\n您的选项代码有误，回复“投票”查看投票说明。";
							$resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
							echo $resultStr;
							return;
						}
					}
				}
				
				//记录投票时间
				$insert_sql="INSERT INTO voted_final(from_user, name, time) VALUES('$fromUsername','$keywords[0]','$nowtime')";
				$res = _insert_data($insert_sql);
				//处理选项，更新vote表中的fakeid项，将count值加1
				FOR ($i = 0; $i < $chosen; $i++) 
				{
					$update_sql="UPDATE vote SET count=count+1 WHERE fakeid='$options[$i]'";
					$res = _update_data($update_sql);
				}
				//投票成功！
				$select_sql="SELECT ID FROM voted_final WHERE from_user='$fromUsername'";
				$visitid=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
				if (!empty($visitid))
					$contentStr = "投票成功！\n您的抽奖编号为：".$visitid['ID']."\n请不要重复投票哦~";
				
			}
			else
			switch($keyword)
			{
				case "0":
				case "创意大赛":
					$contentStr = "欢迎参加第五届创意大赛。\n请回复：
【投票】查看微信投票规则。
【介绍】查看活动介绍。
【日程】查看日程安排。
【奖项】查看奖项设置。
【往届作品】查看往届优秀作品。
【本届作品】查看本届作品简介。
【整体介绍】查看本届大赛说明书。";
					break;
				case "介绍":
					$contentStr = "梦想秀是以院系为单位，面向大一新生开展的活动，鼓励每位大一同学大胆说出自己的梦想。第五届创意大赛将于2015年年底举办，全校同学围绕着“校园优化”的主题参与竞赛。“梦想秀·让创意飞翔”是二者的结合，面向大一新生推出创意大赛的新生专场，助力同学的创意梦想。“梦想秀·让创意飞翔”活动在清华梦想秀的框架下，针对一部分同学的校园梦想、创意梦想进行选拔和竞赛，并给予支持。";
					break;
				case "6":
				case "投票":
				    $select_sql="SELECT content FROM info WHERE entry='votetitle'";
					$res=_select_data($select_sql);
					$rows=mysql_fetch_array($res, MYSQL_ASSOC);
					$contentStr = "【".$rows['content']."】\n";
					$select_sql="SELECT content FROM info WHERE entry='votedesc'";
					$res=_select_data($select_sql);
					$rows=mysql_fetch_array($res, MYSQL_ASSOC);
					$contentStr = $contentStr.$rows['content'];
					$select_sql="SELECT fakeid, content FROM vote";
					$res=_select_data($select_sql);
					while ($rows=mysql_fetch_array($res, MYSQL_ASSOC))
					{
						$contentStr = $contentStr."\n"."【".$rows['fakeid']."】".$rows['content'];
					}
					break;
				case "奖项":
					$contentStr = "梦想秀•让创意飞翔暨第五届创意大赛决赛答辩将产生：
1）一等奖1名：奖金3000元
2）二等奖2名：奖金2000元
3）三等奖3名：奖金1000元
4）最佳人气奖1名： 奖金1000元
所有院系提交的推荐作品（包括没有进入决赛的作品）均可以申报SRT挑战杯专项，获得最高2000元的专项基金支持。
另外，所有报名参加活动的同学还将获得优先进入校学生科协的机会。";
					break;
				case "日程":
					$msgTpl="<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<ArticleCount>1</ArticleCount>
<Articles>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
</Articles>
</xml> ";
					$resultStr = sprintf($msgTpl, $fromUsername, $toUsername, $time, "news", "日程安排", "点击查看本届创意大赛日程安排", webroot."files/images/richeng.png", webroot."thusast/schedule2015.html");
					echo $resultStr;
					return;
					break;
				case "往届作品":
										$msgTpl="<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<ArticleCount>5</ArticleCount>
<Articles>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
</Articles>
</xml> ";
					$resultStr = sprintf($msgTpl, $fromUsername, $toUsername, $time, "news", "激活沉睡空间——宿舍床位空间的功能性优化改造", "", webroot."files/images/past-1.jpg", webroot."thusast/past-1.html", "校园操场防盗寄存系统开发方案", "", webroot."files/images/past-2.jpg", webroot."thusast/past-2.html", "六教引导标识系统改善计划", "", webroot."files/images/past-3.png", webroot."thusast/past-3.html", "可以暖手的保温杯", "", webroot."files/images/past-4.png", webroot."thusast/past-4.html", "下一个优秀作品？期待你的参赛~", "", webroot."files/images/cy.png?20150905", webroot."index.html");
					echo $resultStr;
					return;
					break;
				case "整体介绍":
					$msgTpl="<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<ArticleCount>1</ArticleCount>
<Articles>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
</Articles>
</xml> ";
					$resultStr = sprintf($msgTpl, $fromUsername, $toUsername, $time, "news", "整体介绍", "点击查看本届创意大赛说明书", webroot."files/images/cy.png?20150905", webroot."thusast/instructions.html");
					echo $resultStr;
					return;
					break;
				case "本届作品":
					$contentStr = "【本届作品】\n请点击链接：\n<a href=\"".webroot."index.html\">梦想秀·让创意飞翔</a>";
					break;
				case "1";
					$contentStr = "清华大学学生科学技术协会（简称学生科协），于1983年成立，是清华大学唯一的覆盖文、理、工、医、艺的全校性学生学术科技组织，在全校所有本科院系设有院系基层科协或学生学术活动中心、艺术协会等同类组织。学生科协的英文简称为THUSAST(Student Association for Science and Technology，Tsinghua University)。前身科学会于1915年成立，由当时在校学生，后成为著名物理学家的叶企孙先生倡导创办。
http://www.thusast.org/";
					break;
				case "2";
					$msgTpl="<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<ArticleCount>1</ArticleCount>
<Articles>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
</Articles>
</xml> ";
					$resultStr = sprintf($msgTpl, $fromUsername, $toUsername, $time, "news", "部门介绍", "点击查看校科协部门介绍", webroot."files/images/kx.jpg", webroot."thusast/department.html");
					echo $resultStr;
					return;
					break;
				case "3";
					$msgTpl="<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<ArticleCount>1</ArticleCount>
<Articles>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
</Articles>
</xml> ";
					$resultStr = sprintf($msgTpl, $fromUsername, $toUsername, $time, "news", "大型活动", "点击查看大型活动介绍", webroot."files/images/kz.jpg", webroot."thusast/activity.html");
					echo $resultStr;
					return;
					break;
				case "4";
					$contentStr = "【活动预告】\n第五届清华大学创意大赛正在火热进行中，回复【创意大赛】获得更多新鲜资讯！";
					break;
				case "5";
					$msgTpl="<xml>
<ToUserName><![CDATA[%s]]></ToUserName>
<FromUserName><![CDATA[%s]]></FromUserName>
<CreateTime>%s</CreateTime>
<MsgType><![CDATA[%s]]></MsgType>
<ArticleCount>1</ArticleCount>
<Articles>
<item>
<Title><![CDATA[%s]]></Title> 
<Description><![CDATA[%s]]></Description>
<PicUrl><![CDATA[%s]]></PicUrl>
<Url><![CDATA[%s]]></Url>
</item>
</Articles>
</xml> ";
					$resultStr = sprintf($msgTpl, $fromUsername, $toUsername, $time, "news", "挑战是一种信仰", "【学生科协专刊】挑战是一种信仰", webroot."files/images/itz.jpg", webroot."thusast/itz.html");
					echo $resultStr;
					return;
					break;
				case "帮助";
					$contentStr = "感谢您关注清华大学学生科协！
在这里您将获得最全面最及时的学生科技活动的信息，接触到最好玩最炫酷的学生科协团队，学生科协会陪你一起成长，一起成才~
回复相关数字可查询对应资讯：
【0】创意大赛专场
【1】学生科协简介
【2】部门介绍
【3】大型活动
【4】活动预告
【5】挑战是一种信仰
【6】本届大赛投票规则
<a href=\"http://weixin.thusast.org/index.html\">梦想秀·让创意飞翔</a>
（回复【帮助】重现此消息）";
					break;
				default:
					//截取第一个字符
					$str = mb_substr($keywords[0],0,1);
					//截取剩下的字符
					$str2 = mb_substr($keywords[0],1,strlen($keywords[0])-1);
					if ((($str=='A')||($str=='B')||($str=='C')||($str=='D'))&&(intval($str2,10)))
					{
						$contentStr = "Category ".$str."\n-><a href=\"".webroot.$str."-".$str2.".html\">第".$str2."页</a><-";
					}
					else $contentStr = "感谢您关注清华大学学生科协！
在这里您将获得最全面最及时的学生科技活动的信息，接触到最好玩最炫酷的学生科协团队，科协THU会陪你一起成长，一起成才~
回复相关数字可查询对应资讯：
【0】创意大赛专场
【1】学生科协简介
【2】部门介绍
【3】大型活动
【4】活动预告
【5】挑战是一种信仰
【6】本届大赛投票规则
<a href=\"http://weixin.thusast.org/index.html\">梦想秀·让创意飞翔</a>
（回复【帮助】重现此消息）";
					break;
			}
            $resultStr = sprintf($textTpl, $fromUsername, $toUsername, $time, $msgType, $contentStr);
            echo $resultStr;
        }else
		{
            echo "Input something...";
		}

    }

    public function handleEvent($object)
    {
        $contentStr = "";
        switch ($object->Event)
        {
            case "subscribe":
			//处理订阅事件
                $contentStr = "感谢您关注清华大学学生科协！
在这里您将获得最全面最及时的学生科技活动的信息，接触到最好玩最炫酷的学生科协团队，科协THU会陪你一起成长，一起成才~
回复相关数字可查询对应资讯：
【0】创意大赛专场
【1】学生科协简介
【2】部门介绍
【3】大型活动
【4】活动预告
【5】挑战是一种信仰
【6】本届大赛投票规则
<a href=\"http://weixin.thusast.org/index.html\">梦想秀·让创意飞翔</a>
（回复【帮助】重现此消息）";
                break;
			case "unsubscribe":
			//退订事件
                $contentStr = "再见！";
                break;
            default :
                $contentStr = "Unknow Event: ".$object->Event;
                break;
        }
        $resultStr = $this->responseText($object, $contentStr);
        return $resultStr;
    }
    
    public function responseText($object, $content, $flag=0)
    {
        $textTpl = "<xml>
                    <ToUserName><![CDATA[%s]]></ToUserName>
                    <FromUserName><![CDATA[%s]]></FromUserName>
                    <CreateTime>%s</CreateTime>
                    <MsgType><![CDATA[text]]></MsgType>
                    <Content><![CDATA[%s]]></Content>
                    <FuncFlag>%d</FuncFlag>
                    </xml>";
        $resultStr = sprintf($textTpl, $object->FromUserName, $object->ToUserName, time(), $content, $flag);
        return $resultStr;
    }

     private function checkSignature()
	{
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];	
        		
		$token = TOKEN;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
	
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
			}
	}
    
}

?>
