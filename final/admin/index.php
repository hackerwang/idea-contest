<?php
header("Content-type: text/html; charset=utf-8"); 
require_once '../../mysql_func.php';
ini_set("display_errors", 0); 
ini_set('date.timezone','Asia/Shanghai'); 

if(@$_POST['ac']=="addoption")
{
	$insert_sql="INSERT INTO vote(fakeid, content, count) VALUES('$_POST[fakeid]', '$_POST[content]', '0')";
	$res = _insert_data($insert_sql);
	if ($res==1)
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"插入成功！\");";  
		echo "</script>";  
        $url = "./index.php";  
		echo "<script language='javascript' type='text/javascript'>";  
		echo "window.location.href='$url'";  
		echo "</script>";  
		exit;
	}
	else
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"插入失败！请检查选项代码是否重复。\");";  
		echo "</script>"; 
	}
}
else if(@$_POST['ac']=="updateoption")
{
	$update_sql="UPDATE vote SET fakeid='$_POST[fakeid]', content='$_POST[content]', count='$_POST[count]' WHERE ID='$_POST[optionid]'";
	$res = _update_data($update_sql);
	if ($res>=1)
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"ID:".$_POST['optionid']." 记录更新成功！\");";  
		echo "</script>";
        $url = "./index.php";  
		echo "<script language='javascript' type='text/javascript'>";  
		echo "window.location.href='$url'";  
		echo "</script>";  
		exit;
	}
	else
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"ID:".$_POST['optionid']." 记录更新失败！\");";  
		echo "</script>"; 
	}
}
else if(!empty($_GET['delid']))
{
	$delete_sql="DELETE FROM vote WHERE ID='$_GET[delid]'";
    $res = _delete_data($delete_sql);
	if ($res>=1)
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"ID:".$_GET['delid']." 记录已经删除！\");";  
		echo "</script>";
        $url = "./index.php";  
		echo "<script language='javascript' type='text/javascript'>";  
		echo "window.location.href='$url'";  
		echo "</script>";  
		exit;
	}
	else
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"ID:".$_GET['delid']." 记录删除失败！\");";  
		echo "</script>"; 
	}
}
else if(@$_GET['cleardata']=="yes")
{
	$delete_sql="DELETE FROM voted_final WHERE 1";
    $res1 = _delete_data($delete_sql);
	$update_sql="UPDATE vote SET count='0'";
	$res2 = _update_data($update_sql);
	if (($res1>=1)&&($res2>=1))
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"统计数据已清空！\");";  
		echo "</script>";
        $url = "./index.php";  
		echo "<script language='javascript' type='text/javascript'>";  
		echo "window.location.href='$url'";  
		echo "</script>";  
		exit;
	}
	else
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"统计数据清空失败！\");";  
		echo "</script>"; 
	}
}
else if(@$_POST['ac']=="updateinfo")
{
	$update_sql="UPDATE info SET content='$_POST[votetitle]' WHERE entry='votetitle'";
	$res1 = _update_data($update_sql);
	$update_sql="UPDATE info SET content='$_POST[votedesc]' WHERE entry='votedesc'";
	$res2 = _update_data($update_sql);
	$update_sql="UPDATE number SET value='$_POST[mustchoose]' WHERE entry='mustchoose'";
	$res3 = _update_data($update_sql);
	$update_sql="UPDATE number SET value='$_POST[mostchoose]' WHERE entry='mostchoose'";
	$res5 = _update_data($update_sql);
	if (@$_POST['permitmore'])
	{
		$update_sql="UPDATE number SET value='1' WHERE entry='permitmore'";
		$res4 = _update_data($update_sql);
	}
	else
	{
		$update_sql="UPDATE number SET value='0' WHERE entry='permitmore'";
		$res4 = _update_data($update_sql);
	}
	if (@$_POST['voteswitch'])
	{
		$update_sql="UPDATE number SET value='1' WHERE entry='voteswitch'";
		$res6 = _update_data($update_sql);
	}
	else
	{
		$update_sql="UPDATE number SET value='0' WHERE entry='voteswitch'";
		$res6 = _update_data($update_sql);
	}
	if (($res1>=1)&&($res2>=1)&&($res3>=1)&&($res4>=1)&&($res5>=1)&&($res6>=1))
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"投票设置更新成功！\");";  
		echo "</script>";
        $url = "./index.php";  
		echo "<script language='javascript' type='text/javascript'>";  
		echo "window.location.href='$url'";  
		echo "</script>";  
		exit;
	}
	else
	{
		echo "<script language='javascript' type='text/javascript'>";  
		echo "alert(\"投票设置更新失败！\");";  
		echo "</script>"; 
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="keywords" content="" />
		<meta name="description" content="" />
<?php
$select_sql="SELECT content FROM info WHERE entry='sysname'";
$select_res=_select_data($select_sql);
$rows=mysql_fetch_assoc($select_res);
$sysname = $rows['content'];
echo "<title>".$sysname." - 投票管理</title>"
?>
		<script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/global.js" type="text/javascript" charset="utf-8"></script>
		<script src="js/modal.js" type="text/javascript" charset="utf-8"></script>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" charset="utf-8" />
		<!--[if IE 6]>
			<link rel="stylesheet" href="css/ie6.css" type="text/css" media="screen" charset="utf-8" />
		<![endif]-->		
	</head>
	<body>
		<div id="header">
			<div class="col w5 bottomlast">
				<a href="" class="logo">
					<img src="images/logo.gif" alt="Logo" />
				</a>
			</div>
			<div class="col w5 last right bottomlast">
				<p class="last"><strong><?php echo $_SERVER['PHP_AUTH_USER']; ?></strong> 已登录，关闭浏览器后自动注销。</p>
			</div>
			<div class="clear"></div>
		</div>
		<div id="wrapper">
			<div id="minwidth">
				<div id="holder">
					<div id="menu">
						<div id="left"></div>
						<div id="right"></div>
						<ul>
							<li>
								<a href="index.php"  class="selected"><span>Vote</span></a>
							</li>
                            <li>
								<a href="../chart.jpg" target="_blank"><span>Chart</span></a>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
					<div id="submenu">
						<div class="modules_left">
							<div class="module buttons">
								<a href="" class="dropdown_button"><small class="icon plus"></small><span>Add Option</span></a>
								<div class="dropdown">
									<div class="arrow"></div>
									<div class="content">
										<form action="index.php" method="post">
											<p>
                                            	<input type="hidden" name="ac" value="addoption" />
												<label for="fakeid">选项代码(限1个字节,数据库不区分大小写):</label>
												<input type="text" class="text w_22" name="fakeid" id="fakeid" value="" />
											</p>
											<p>
												<label for="content">选项描述:</label>
												<input type="text" class="text w_22" name="content" id="content" value="" />
											</p>
                                            <a href="" class="button green right form_submit"><small class="icon check"></small><span>Submit</span></a>
                                            <input type="submit" value="submit" class="novisible" />
                                            <a class="button red mr right close"><small class="icon cross"></small><span>Close</span></a>
										</form>
										
										
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="title">
							投票管理
						</div>
						<div class="modules_right">
							<div class="module buttons">
                            <p class="buttons_demo"><a href="javascript:location.reload(true)" class="button"><small class="icon crosshair"></small><span>Refresh</span></a></p>
                            </div>
						</div>
					</div>
					<div id="desc">
						<div class="body">
							<div class="col w2">
                            	<div class="content">
									<div class="box header">
										<div class="head"><div></div></div>
										<h2>Tips</h2>
										<div class="desc">
											<p>· 向微信公众号回复“投票”即可查看投票说明，回复“姓名+xxx”（x为选项代码）即可进行投票。</p><br />
											<p>· 选项代码与数据库中存储位置ID无关，可自行设定，但是不得超过<font color="red">1个字节</font>！</p><br />
                                            <p>· 点击上方“Add Option”可以增加选项。</p><br />
                                            <p>· 右侧可以查看并修改投票结果。</p><br />
                                            <p>· 暂不支持增加投票。</p><br />
                                            <p>· 描述中尽量使用中文标点，避免使用英文引号等。</p><br />
											<p>· 为达到最佳浏览效果，请使用<font color="red">1366*768或更高</font>分辨率浏览此页。</p><br />
										</div>
										<div class="bottom"><div></div></div>
									</div>
								</div>
                                <div class="content">
									<div class="box header">
										<div class="head"><div></div></div>
										<h2>Chart</h2>
										<div class="desc">
											<!--<p align="center"><img src="chart.php" height="80" alt=""/></p>-->
											<br />
                                            <p>· 统计图表地址：<br/></p>
											<form>
												<label for="addr">统计图：</label>
												<input type="text" class="text w_15" name="addr" id="addr" value="http://weixin.thusast.org/final/chart.jpg" />
											</form>
                                            <br />
									  </div>
										<div class="bottom"><div></div></div>
									</div>
								</div>
								<div class="content">
									<div class="box header">
										<div class="head"><div></div></div>
										<h2>Total</h2>
										<div class="desc">
											<p>&nbsp;</p>
                                            <p>· 已投票用户数：<?php
$select_sql="SELECT count(*) FROM voted_final";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
echo $rows['count(*)'];				
?>
人</p><br />
											<p>&nbsp;</p>
										</div>
										<div class="bottom"><div></div></div>
									</div>
								</div>
							</div>
							<div class="col w8 last">
								<div class="content">
                                	<div class="box header">
										<div class="head"><div></div></div>
										<h2>投票设置</h2>
										<div class="desc">
											<form action="index.php" method="post">
                                            <input type="hidden" name="ac" value="updateinfo" />
										  		<table>
												<tr>
													<th width="20%"><label for="votetitle">投票标题</label></th>
													<td width="80%"><input type="text" class="text w_22" name="votetitle" id="votetitle" value="<?php
$select_sql="SELECT content FROM info WHERE entry='votetitle'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
echo $rows['content'];		
?>" /></td>
												</tr>
                                                <tr id="id_1">
                                                	<th><label for="votedesc">投票说明</label></th>
													<td><input type="text" class="text w_40" name="votedesc" id="votedesc" value="<?php
$select_sql="SELECT content FROM info WHERE entry='votedesc'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
echo $rows['content'];	
?>" /></td>
												</tr>
												<tr id="id_2">
													<th><label for="mustchoose">必选项数</label></th>
													<td><input type="text" class="text w_22" name="mustchoose" id="mustchoose" value="<?php
$select_sql="SELECT value FROM number WHERE entry='mustchoose'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
echo $rows['value'];	
?>" /></td>
												</tr>
												<tr id="id_3">
													<th><label for="mustchoose">最多项数</label></th>
													<td><input type="text" class="text w_22" name="mostchoose" id="mostchoose" value="<?php
$select_sql="SELECT value FROM number WHERE entry='mostchoose'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
echo $rows['value'];	
?>" /></td>
												</tr>
												<tr id="id_4">
													<th><label for="permitmore">允许多选</label></th>
													<td><input type="checkbox" class="checkbox" name="permitmore" id="permitmore" <?php
$select_sql="SELECT value FROM number WHERE entry='permitmore'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
if($rows['value']==1) echo "checked=\"checked\""; else echo ""; ?> /></td>
												</tr>
												<tr id="id_5">
													<th><label for="permitmore">投票开关</label></th>
													<td><input type="checkbox" class="checkbox" name="voteswitch" id="voteswitch" <?php
$select_sql="SELECT value FROM number WHERE entry='voteswitch'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
if($rows['value']==1) echo "checked=\"checked\""; else echo ""; ?> /></td>
												</tr>
												</table>
                                   				<input type="submit" value="submit" class="novisible" />
                                    			<a href="" class="button green right form_submit"><small class="icon check"></small><span>Update Infomation</span></a>
                                    		</form>
                                            <br /><br />
                                            <a href="index.php?cleardata=yes" class="button red right" onClick="return confirm('确认要清空统计数据吗？\n注意这不会删除已有选项。');"><small class="icon cross"></small><span>Clear Statistics</span></a>
                                            <br />
                                            
								      </div>
                                        
									  <div class="bottom"><div></div></div>
									</div>
                                
									<table>
										<tr>
											<th width="10%">序号</th>
											<th width="10%">选项代码</th>
											<th width="40%">选项描述</th>
											<th width="10%">普通计数</th>
                                            <th width="15%">更新选项</th>
                                            <th width="15%">删除选项</th>
										</tr>
                                    </table>
<?php
$select_sql="SELECT * FROM `vote` ORDER BY `ID` ASC";
$res=_select_data($select_sql);
while ($rows=mysql_fetch_array($res, MYSQL_ASSOC))
{
?>
											<form action="index.php" method="post">
                                                <table>
                                                <tr id="id_<?php echo $rows['ID']; ?>">
                                            	<td width="10%"><?php echo $rows['ID']; ?></td>
                                            	<td width="10%"><input type="text" class="text w_5" name="fakeid" id="fakeid" value="<?php echo $rows['fakeid']; ?>" /></td>
                                            	<td width="40%">
                                                    <input type="text" class="text w_30" name="content" id="content" value="<?php echo $rows['content']; ?>" />
                                                    <input type="hidden" name="ac" value="updateoption" />
                                            		<input type="hidden" name="optionid" value="<?php echo $rows['ID']; ?>" />
                                            		<input type="submit" value="submit" class="novisible" />
                                                </td>
                                            	<td width="10%"><input type="text" class="text w_5" name="count" id="count" value="<?php echo $rows['count']; ?>" /></td>
                                            	<td width="15%"><a href="" class="button green form_submit"><small class="icon check"></small><span>Update</span></a></td>
                                            	<td width="15%"><a href="index.php?delid=<?php echo $rows['ID']; ?>" class="button red" onclick="return confirm('确认要删除ID为<?php  echo $rows['ID']; ?>的记录吗？');"><small class="icon cross"></small><span>Delete</span></a></td>
                                                </tr>
                                                </table>
                                            </form>
                                            
<?php 
}
?>
										
								</div>		
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div id="body_footer">
							<div id="bottom_left"><div id="bottom_right"></div></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer">
			<p class="last">Copyright &copy; 2014 - Version: <?php
$select_sql="SELECT content FROM info WHERE entry='version'";
$res=_select_data($select_sql);
$rows=mysql_fetch_array($res, MYSQL_ASSOC);
echo $rows['content'];	
?>
 - 共青团清华大学电子工程系委员会组织组·社工办 - Created by <a href="http://0degree.lofter.com" target="_blank">Han Wang</a></p>
		</div>	
	</body>
</html>