<!DOCTYPE html>
<?php
header("Content-Type: text/html; charset=uft-8");
ini_set('date.timezone','Asia/Shanghai');
//获取当前页码，若未指定则为1
if (empty($_GET['page']))
	$nowpage = 1;
else $nowpage = $_GET['page'];
//获取作品总数
require_once 'mysql_func.php';
$select_sql="SELECT count(*) FROM ideas";
$rows=mysql_fetch_array(_select_data($select_sql), MYSQL_ASSOC);
//计算总页码数
$total = (int)($rows['count(*)'] / 10);
if ($rows['count(*)'] % 10 != 0) $total++;
if ($nowpage>$total)
{
	@header("http/1.1 404 not found"); 
	@header("status: 404 not found"); 
	include($_SERVER['DOCUMENT_ROOT']."/errorpages/error404.html");//跳转到404
	exit(); 
}
?>
<html>
<head>
<meta http-equiv="content-type" content="text/html" charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0;" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<title>梦想秀·让梦想飞翔</title>
<link href="styles/bootstrap.min.css" rel="stylesheet" />
<link href="styles/Global.css?20150904" rel="stylesheet" />
</head>
<body>
 <div class="header">
 <a href="index.html" class="home">
            <span class="header-icon header-icon-home"></span>
            <span class="header-name">主页</span>
</a>
<div class="title" id="titleString">全部作品</div>
<a href="javascript:history.go(-1);" class="back">
            <span class="header-icon header-icon-return"></span>
            <span class="header-name">返回</span>
        </a>
 </div>


 <div class="container">
 <div id="BookRoom" class="tab-pane active fade in">   
<div class="detail-address-bar">
  <p>浏览全部作品</p>
</div>
<div id="datetab" class="detail-time-bar">
  <p>共找到记录：<?php
echo $rows['count(*)'];
?>条<br />当前是第<?php echo $nowpage;?>页，共<?php
echo $total;
?>页</p>
</div>  

<ul class="unstyled roomlist">
<?php
$select_sql="SELECT category, fakeid, dept, name, title, content, count FROM ideas WHERE 1 ORDER BY `ideas`.`fakeid` ASC LIMIT ".(($nowpage-1)*10).",10";
$res=_select_data($select_sql);
while ($rows=mysql_fetch_array($res, MYSQL_ASSOC))
{  
?>
	<li>
        <div class="roomtitle" style="height:100%;">
        <div class="roomname"><?php
$select_sql="SELECT description FROM category WHERE category='$rows[category]'";
$description=mysql_fetch_array(_select_data($select_sql),MYSQL_ASSOC);
echo "<a style=\"color:#60ab2b\"href=\"detail-".$rows['fakeid'].".html\">【".$description['description']."】".$rows['title']."</a>";
?></div>
        <div class="fr"><em class="orange roomprice">作品代码：<?php echo $rows['fakeid'];?><br>得票数：<?php echo $rows['count'];?></em></div>
        <div><i>[<?php echo $rows['dept']." ".$rows['name'];?>]</i><br /><?php echo $rows['content'];?></div>
        </div>
    </li> 
<?php
}
?>
</ul>
<div style="transform-origin: 0px 0px 0px; opacity: 1; transform: scale(1, 1);" class="hotel-prompt">
	<div class="page" style="text-align:center;">
		<a class="nextprebutton" <?php if ($nowpage>1) echo "href=\"all-".($nowpage-1).".html\""; ?> style="margin-right:5px;">Previous</a>
		<?php
FOR ($i = 1; $i <= $total; $i++)
{
	if ($i!=$nowpage) echo "<a href=\"all-".$i.".html\" style=\"margin-right:5px;\">".$i."</a>";
	else echo "<span class=\"currentpage\" style=\"margin-right:5px;\">".$i."</span>";
	if ($i%8==0) echo "<br />";
}
		?>
		<a class="nextprebutton" <?php if ($nowpage<$total) echo "href=\"all-".($nowpage+1).".html\"";?> style="margin-right:5px;">Next</a>
		<br /><br /><a class="nextprebutton" href="javascript:scroll(0,0)">Top</a>
	</div>
</div>

</div> 
</div>

  <?php
  include 'footer.php';
  ?>

</body>
</html>
